package com.ing.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ing.dao.ILoginDao;

@Service
public class LoginServiceImpl implements ILoginService{
	@Autowired
	private ILoginDao loginDao;
	@Override
	public String validateUser(String userName, String passWord) {
		// TODO Auto-generated method stub
		return loginDao.validateUser(userName, passWord);
	}

}
