package com.ing.dao;

import org.springframework.stereotype.Repository;

import com.ing.util.CommonConstants;

@Repository
public class LoginDaoImpl implements ILoginDao {

	@Override
	public String validateUser(String userName, String passWord) {
		if(CommonConstants.USERNAME.equals(userName)) {
			return "valid User";
		}else {
			return "Invalid User";
		}
	}

}
