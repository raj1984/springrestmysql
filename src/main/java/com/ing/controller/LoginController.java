package com.ing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ing.service.ILoginService;
import com.ing.util.CustomExceptinClass;

@RestController
public class LoginController {
	@Autowired
	private  ILoginService loginService;

	@RequestMapping(value="/user")
	public String getLoginAuthentication(@RequestParam("userName") String userName,
										 @RequestParam("passWord") String passWord) throws CustomExceptinClass {
		String validUser="";
		try {
				validUser=loginService.validateUser(userName, passWord);
		}catch(Exception e) {
			throw new CustomExceptinClass(e.getMessage());
		}
		
		return "USER--"+userName+"---"+"validUser";
	}
}
